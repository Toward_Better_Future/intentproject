package com.ziauddinmasoomi.calculatro3;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    private Intent intent ;
    private EditText nameF,genderF,salaryF;
    private TextView infoText;
    private Button sendBtn;
    private boolean isEmpty;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    @Override
    protected void onStart() {
        super.onStart();

        nameF = (EditText) findViewById(R.id.nameText);
        genderF = (EditText) findViewById(R.id.genderText);
        salaryF = (EditText) findViewById(R.id.salaryText);
        sendBtn = (Button) findViewById(R.id.sendBtn);
        infoText = (TextView) findViewById(R.id.infoText);


        sendBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                isEmpty = nameF.getText().toString().trim().equals("") || genderF.getText().toString().trim().equals("") ||
                        salaryF.getText().toString().trim().equals("") ;

                if (isEmpty){
                    infoText.setText("Fill all fields");
                }
                else {
                    intent = new Intent(MainActivity.this,resultActivity.class);

                    intent.putExtra("name",nameF.getText().toString());
                    intent.putExtra("gender",genderF.getText().toString());
                    intent.putExtra("salary",salaryF.getText().toString());

                    startActivity(intent);

                    nameF.setText("");
                    genderF.setText("");
                    salaryF.setText("");
                }
            }
        });
    }
}
