package com.ziauddinmasoomi.calculatro3;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

public class resultActivity extends AppCompatActivity {

    private Intent intent;
    private String name,gender,salary,message ,title;
    private TextView messageF;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_result);
    }

    @Override
    protected void onStart() {
        super.onStart();

        intent = getIntent();
        name = intent.getStringExtra("name");
        gender = intent.getStringExtra("gender");
        salary = intent.getStringExtra("salary");

        if (gender.equalsIgnoreCase("male")){
            title = "Mr.";
        }
        else if (gender.equalsIgnoreCase("female")){
            title = "Mrs.";
        }
        else {
            title = "";
        }

        message = "Dear "+title+name+" your salary is "+salary+"$";
        messageF = (TextView)findViewById(R.id.detailsText);
        messageF.setText(message);
    }
}
